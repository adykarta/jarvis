from gtts import gTTS
import speech_recognition as sr
import os
import re
import webbrowser
import smtplib
import sys 
import requests

def talkToMe(audio):
    text_to_speech = gTTS(text=audio, lang='en')
    text_to_speech.save('audio.mp3')
    os.system('mpg123 audio.mp3')

def myCommand():
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print('Ready for next command...')
        r.pause_threshold = 1
        r.adjust_for_ambient_noise(source, duration=1)
        audio = r.listen(source)

    try:
        command = r.recognize_google(audio).lower()
        print('You said: ' + command + '\n')

    except sr.UnknownValueError:
        talkToMe('I am sorry, Can you repeat that?')
        print('Your last command couldn\'t be heard')
        command = myCommand()

    return command


def assistant(command):
    if 'open website' in command:
        reg_ex = re.search('open website (.+)', command)
        if reg_ex:
            domain = reg_ex.group(1)
            url = 'https://www.' + domain
            webbrowser.open(url)
            print('Done!')
        else:
            pass

    elif 'okay what\'s up' in command:
        talkToMe('Just doing my thing')
    
    elif 'what if i say i love you' in command:
        talkToMe('What the fuck?')
    
    
    elif 'open instagram' in command:
        reg_ex = re.search('open instagram (.+)', command)
        url = 'https://www.instagram.com/'
        if reg_ex:
            subinstagram = reg_ex.group(1)
            url =  url + subinstagram
            talkToMe('opening instagram'+ subinstagram)
        else:
            talkToMe('opening instagram')
        webbrowser.open(url)
        print('Done!')
    
    elif 'sing me a song about her' in command:
        talkToMe('which one?')
        answer = myCommand()
        if 'someone' in answer:
            talkToMe('as you wish')
            url = 'https://www.youtube.com/watch?v=mzB1VGEGcSU'
            webbrowser.open(url)

    elif 'open c ak' in command:
        url = 'https://www.academic.ui.ac.id'
        talkToMe('are you sure?')
        voice = myCommand()
        if 'yes' in voice:
            webbrowser.open(url)
            print('Done!')
        else:
            print('Canceling..')

    elif 'okay good night' in command:
        talkToMe('good night')
        webbrowser.close()
        sys.exit()
    

talkToMe('Hello Ady! how can i help you?')

while True:
    assistant(myCommand())